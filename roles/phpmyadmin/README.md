havlasme.webhost.phpmyadmin
===========================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage phpmyadmin deployment.

Requirements
------------

None.

Role Variables
--------------

```yaml
# the phpmyadmin state ('present', 'absent')
phpmyadmin_state: "present"
# the phpmyadmin version
phpmyadmin_version: "5.2.0"
# the phpmyadmin source (must be .tar.gz or .tar.xz archive)
phpmyadmin_source: "https://files.phpmyadmin.net/phpMyAdmin/{{ phpmyadmin_version }}/phpMyAdmin-{{ phpmyadmin_version }}-all-languages.tar.xz"

# the phpmyadmin installation directory
phpmyadmin_directory: "/usr/local/share/phpmyadmin/{{ phpmyadmin_version }}"
# the phpmyadmin httpd user
phpmyadmin_httpd_user: "www-data"
# the phpmyadmin httpd group
phpmyadmin_httpd_group: "www-data"
```

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: all
  tasks:
  - import_role:
      name: havlasme.webhost.phpmyadmin
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
