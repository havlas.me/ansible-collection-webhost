havlasme.webhost.certbot
========================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage certbot service.

Requirements
------------

None.

Role Variables
--------------

```yaml
# the certbot package state ('present', 'latest')
certbot_state: "present"
# start the certbot service at system boot
certbot_service_enabled: "yes"

# the certbot mail
#certbot_mail:
# the certbot http 01 challenge port
certbot_http_01_port: 80

# the certbot certificate list
certbot_certificate: []
# - domain: string
#   challenge: enum('http-01', 'dns-rfc2136') | d('http-01')
#   mail: string | d(certbot_mail)
# * challenge: http-01
#   port: int | d(certbot_http_01_port)
# * challenge: dns-rfc2136
#   credentials: string
#   delegate_to: string | d(omit)
# the certbot hook list
certbot_hook: []
# - dest: string
#   src: string
#   state: enum('present', 'absent') | d('present')
```

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: all
  tasks:
  - import_role:
      name: havlasme.webhost.certbot
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
