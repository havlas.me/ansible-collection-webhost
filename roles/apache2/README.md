havlasme.webhost.apache2
========================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage apache2 service.

Requirements
------------

None.

Role Variables
--------------

```yaml
# the apache2 package state ('present', 'latest')
apache2_state: "present"
# start the apache2 service at system boot
apache2_service_enabled: "yes"
# can ansible restart the apache2 service
apache2_service_restart: "yes"

# the apache2 service user
apache2_runas_user: "www-data"
# the apache2 service group
apache2_runas_group: "www-data"

# the apache2 configuration directory
apache2_confdir: "/etc/apache2"
# the apache2 log directory
apache2_logdir: "/var/log/apache2"

# the apache2 network bind ip
apache2_network_bind: "*"
# the apache2 network port
apache2_network_port: 80
# the apache2 network ssl port
apache2_network_ssl_port: 443

# the apache2 default access log format
apache2_access_log_format: "combined"

# the apache2 snippet list
# * conf.d/mozilla-ssl-config.conf.j2  an ssl configuration based on mozilla ssl-config util
# * snippet.d/mozilla-ssl-config.conf.j2  an ssl configuration based on mozilla ssl-config util
# * snippet.d/redirect-ssl.conf.j2  redirect a non-ssl request to ssl
apache2_snippet: []
# - dest: string
#   src: string
#   state: enum('present', 'absent') | d('present')
# * snippet.d/mozilla-ssl-config.conf.j2
#   level: enum('modern', 'intermediate', 'old') | d('intermediate')

# the apache2 vhost default template
# * vhost.d/webhost.conf.j2  an opinionated virtual host
apache2_vhost_tmpl: "vhost.d/webhost.conf.j2"
# the apache2 vhost list
apache2_vhost: []
# - dest: string
#   src: string | d(apache2_vhost_tmpl)
#   state: enum('present', 'absent')
# * vhost.d/webhost.conf.j2
#   name: string
#   domain: string[] | d(omit)
#   admin: string | d(omit)
#   root: string
#   virtual_root: string | d(omit)
#   ssl: boolean | d('no')
#   redirect_ssl: boolean | d('yes')
#   bind: string | d(apache2_network_bind)
#   port: int | d(apache2_network_port)
#   ssl_port: int | d(apache2_network_ssl_port)
#   error_log: string
#   no_error_log: boolean | d('no')
#   access_log: string
#   access_log_format: string | d(apache2_access_log_format)
#   no_access_log: boolean | d('no')
#   ssl_cert: string
#   ssl_cert_key: string
#   ssl_cert_chain: string | d(omit)
#   snippet: dict[] | d([])
#   - name: string
#     ssl_only: boolean | d('no')

# the apache2 certbot hook list
apache2_certbot_hook:
- { dest: "deploy/reload-apache2.sh", src: "hook/reload-apache2.sh.j2" }
```

Dependencies
------------

[Community.General](https://docs.ansible.com/ansible/latest/collections/community/general/index.html)

```bash
ansible-galaxy collection install community.general
```

Example Playbook
----------------

```yaml
- hosts: all
  tasks:
  - import_role:
      name: havlasme.webhost.apache2
    vars:
# TODO
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
