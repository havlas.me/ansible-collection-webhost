havlasme.webhost.vsftpd
=======================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage vsftpd service.

Requirements
------------

None.

Role Variables
--------------

```yaml
# the vsftpd package state ('present', 'latest')
vsftpd_state: "present"
# start the vsftpd service at system boot
vsftpd_service_enabled: "yes"
# can ansible restart the vsftpd service
vsftpd_service_restart: "yes"
```

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: all
  tasks:
  - import_role:
      name: havlasme.webhost.vsftpd
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
