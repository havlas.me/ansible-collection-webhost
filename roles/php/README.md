havlasme.webhost.php
====================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage php service.

Requirements
------------

None.

Role Variables
--------------

```yaml
# the php package state ('present', 'latest')
php_state: "present"
# start the php service at system boot
php_service_enabled: "yes"
# can ansible restart php service
php_service_restart: "yes"

# the php repository key url
php_repo_key_url:
- "https://packages.sury.org/php/apt.gpg"
# the php repository url
php_repo_url: "deb https://packages.sury.org/php/ {{ ansible_distribution_release }} main"

# the php version ('5.6', '7.0', '7.1', '7.2', '7.3', '7.4', '8.0', '8.1')
php_version: "8.1"

# the php extension list
php_extension: []
# - name: string
#   state: enum('present', 'absent', 'latest') | d(php_state)

# the php pool default template
php_pool_tmpl: "pool.d/generic.conf.j2"
# the php pool list
php_pool: []
```

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: all
  tasks:
  - import_role:
      name: havlasme.webhost.php
    vars:
# TODO
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
