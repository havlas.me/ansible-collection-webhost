havlasme.webhost.apache2_mod_php
================================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage apache2 mod_php module.

Requirements
------------

None.

Role Variables
--------------

```yaml
# the apache2 mod_php state ('present', 'latest')
apache2_mod_php_state: "present"
# the apache2 mod_php version ('5.6', '7.0', '7.1', '7.2', '7.3', '7.4', '8.0', '8.1')
apache2_mod_php_version: "8.1"

# the php extension list
apache2_mod_php_extension: []
# - name: string
#   state: enum('present', 'absent', 'latest') | d(apache2_mod_php_state)
```

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: all
  tasks:
  - import_role:
      name: havlasme.webhost.apache2_mod_php
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
