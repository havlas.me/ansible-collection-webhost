havlasme.webhost.nginx
======================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage [NGINX](https://nginx.org/) service on [Debian](https://www.debian.org/) or [Ubuntu](https://www.ubuntu.com/).


Role Variables
--------------

```yaml
# the nginx package state ('present', 'latest')
nginx_state: "present"
# start the nginx service at system boot
nginx_service_enabled: "yes"
# can ansible restart the nginx service
nginx_service_restart: "yes"

# the nginx repository key url
nginx_repo_key_url:
- "http://nginx.org/keys/nginx_signing.key"
nginx_repo_url: "deb http://nginx.org/packages/{{ ansible_distribution | lower }}/ {{ ansible_distribution_release }} nginx"

# the nginx service user
nginx_runas_user: "www-data"
# the nginx service group
nginx_runas_group: "www-data"
# the nginx default root directory
nginx_root: "/var/www"

# the nginx configuration directory
nginx_confdir: "/etc/nginx"
# the nginx log directory
nginx_logdir: "/var/log/nginx"

# the nginx network port
nginx_network_port: 80
# the nginx network ssl port
nginx_network_ssl_port: 443

# the nginx default access log format
nginx_access_log_format: "main"

# the nginx main configuration file
nginx_conf_main: "{{ nginx_confdir }}/nginx.conf"
# the nginx main configuration template
nginx_conf_tmpl: "nginx.conf.j2"
# the nginx keepalive_timeout
nginx_keepalive_timeout: 65

# the nginx snippet list
# * conf.d/default.conf.j2  a default virtual host, customizable via default.d
# * snippet.d/certbot-acme.conf.j2  a certbot acme challenge
# * snippet.d/favicon.ico.conf.j2  do not log favicon.ico-related requests
# * snippet.d/mozilla-ssl-config.conf.j2  an ssl configuration based on mozilla ssl-config util
# * snippet.d/no-access-log.conf.j2  disable access log
# * snippet.d/robots.txt.conf.j2  do not log robots.txt-related requests
# * snippet.d/upstream.conf.j2  the upstream definition
nginx_snippet: []
# - dest: string
#   src: string
#   state: string
# * conf.d/default.conf.j2
#   domain: string | d('_')
#   port: int | d(nginx_network_port)
#   backlog: int
# * snippet.d/certbot-acme.conf.j2
#   port: int | d(certbot_http_01_port)
#   access_log_format: string | d('main')
#   no_access_log: boolean | d('no')
# * snippet.d/mozilla-ssl-config.conf.j2
#   level: enum('modern', 'intermediate', 'old') | d('intermediate')
# * snippet.d/upstream.conf.j2
#   name: string
#   strategy: string | d(omit)
#   upstream: string[]

# the nginx vhost default template
# * vhost.d/webhost.conf.j2  an opinionated virtual host
nginx_vhost_tmpl: "vhost.d/webhost.conf.j2"
# the nginx vhost list
nginx_vhost: []
# - dest: string
#   src: string | d(nginx_vhost_tmp)
#   root: string | d(omit)
#   certbot: boolean | d('no')
#   snippetable: boolean | d('no')
#   state: enum('present', 'absent') | d('present')
# * vhost.d/webhost.conf.j2
#   name: string
#   domain: string[] | d(omit)
#   ssl: boolean | d(item.certbot | d('no'))
#   redirect_ssl: boolean | d('yes')
#   port: int | d(nginx_network_port)
#   ssl_port: int | d(nginx_network_ssl_port)
#   no_error_log: boolean | d('no')
#   access_log_format: string | d(nginx_access_log_format)
#   no_access_log: boolean | d('no')
#   ssl_cert: string
#   ssl_cert_key: string
#   ssl_cert_chain: string
#   snippet: dict[] | d([])
#   - name: string
#     http: boolean | d('yes')
#     ssl boolean | d('yes')

# the nginx htpasswd list
nginx_htpasswd: []
# - dest: string
#   name: string
#   password: string
#   state: string

# the nginx diffie-hellman parameters
nginx_dhparam: |
  -----BEGIN DH PARAMETERS-----
  MIICCAKCAgEA//////////+t+FRYortKmq/cViAnPTzx2LnFg84tNpWp4TZBFGQz
  +8yTnc4kmz75fS/jY2MMddj2gbICrsRhetPfHtXV/WVhJDP1H18GbtCFY2VVPe0a
  87VXE15/V8k1mE8McODmi3fipona8+/och3xWKE2rec1MKzKT0g6eXq8CrGCsyT7
  YdEIqUuyyOP7uWrat2DX9GgdT0Kj3jlN9K5W7edjcrsZCwenyO4KbXCeAvzhzffi
  7MA0BM0oNC9hkXL+nOmFg/+OTxIy7vKBg8P+OxtMb61zO7X8vC7CIAXFjvGDfRaD
  ssbzSibBsu/6iGtCOGEfz9zeNVs7ZRkDW7w09N75nAI4YbRvydbmyQd62R0mkff3
  7lmMsPrBhtkcrv4TCYUTknC0EwyTvEN5RPT9RFLi103TZPLiHnH1S/9croKrnJ32
  nuhtK8UiNjoNq8Uhl5sN6todv5pC1cRITgq80Gv6U93vPBsg7j/VnXwl5B0rZp4e
  8W5vUsMWTfT7eTDp5OWIV7asfV9C1p9tGHdjzx1VA0AEh/VbpX4xzHpxNciG77Qx
  iu1qHgEtnmgyqQdgCpGBMMRtx3j5ca0AOAkpmaMzy4t6Gh25PXFAADwqTs6p+Y0K
  zAqCkc3OyX3Pjsm1Wn+IpGtNtahR9EGC4caKAH5eZV9q//////////8CAQI=
  -----END DH PARAMETERS-----

# the nginx certbot hook list
nginx_certbot_hook:
- { dest: "deploy/reload-nginx.sh", src: "hook/reload-nginx.sh.j2" }
```


Dependencies
------------

[Community.General](https://docs.ansible.com/ansible/latest/collections/community/general/index.html)

```bash
ansible-galaxy collection install community.general
```


Example Playbook
----------------

```yaml
- hosts: 'all'
  
  tasks:
  - ansible.builtin.include_role:
      name: 'havlasme.webhost.nginx'
```


License
-------

Apache-2.0


Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
