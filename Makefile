NAMESPACE = $(shell yq -r .namespace galaxy.yml)
ROLENAME = $(shell yq -r .name galaxy.yml)
ROLEVERSION = $(shell yq -r .version galaxy.yml)
DIST = ./dist
GALAXY = @ansible-galaxy

.PHONY: build
build:
	$(GALAXY) collection build --output-path "$(DIST)"

.PHONY: clean
clean:
	-rm --recursive "$(DIST)"

.PHONY: install
install:
ifeq (, $(shell which yq))
	$(error "no yq. try doing pip3 install yq")
else
	$(GALAXY) collection install "$(DIST)/$(NAMESPACE)-$(ROLENAME)-$(ROLEVERSION).tar.gz"
endif

.PHONY: lint
lint:
	$(MAKE) -C roles/apache2 lint
	$(MAKE) -C roles/apache2_mod_php lint
	$(MAKE) -C roles/nginx lint
	$(MAKE) -C roles/php lint
	$(MAKE) -C roles/phpmyadmin lint

.PHONY: test
test:
	$(MAKE) -C roles/apache2 test
	$(MAKE) -C roles/apache2_mod_php test
	$(MAKE) -C roles/nginx test
	$(MAKE) -C roles/php test
	$(MAKE) -C roles/phpmyadmin test
