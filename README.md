Ansible Collection - havlasme.webhost
=====================================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) collection of webhosting related roles, plugins, and modules:

- [havlasme.webhost.apache2](/roles/apache2/README.md)
- [havlasme.webhost.apache2_mod_php](/roles/apache2_mod_php/README.md)
- [havlasme.webhost.certbot](/roles/certbot/README.md)
- [havlasme.webhost.nginx](/roles/nginx/README.md)
- [havlasme.webhost.php](/roles/php/README.md)
- [havlasme.webhost.phpmyadmin](/roles/phpmyadmin/README.md)
- [havlasme.webhost.vsftpd](/roles/vsftpd/README.md)


Installation
------------

```bash
ansible-galaxy collection install havlasme.webhost
```

```yaml title="requirements.yml"
---
collections:
- name: havlasme.webhost
```


Development
-----------

```shell
make build
```

```shell title="ansible-lint"
make lint
```

```shell title="molecule"
make test
```


License
-------

Apache-2.0


Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: LICENSE
